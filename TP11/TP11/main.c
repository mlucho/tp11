/*HEADERS*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#include <allegro5/allegro_color.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>        //IMPORTANTE PARA LAS FORMAS

#include "op_bits.h"

/*MACROS DE COORDENAS DE LEDs*/
#define COORD_Xi 80
#define COORD_X_STEP 120
#define COORD_Y 350
#define LED_RAD 50

#define FPS    60.0 //¿Por que se uso double aca ? 
#define COLOR_INC_RATE 1
#define MAX_COLOR_VALUE 255

int main(int argc, char** argv) 
{
    /*Punteros a los elementos*/
    ALLEGRO_DISPLAY *display = NULL; //puntero al display que vamos a usar
    ALLEGRO_EVENT_QUEUE *event_queue = NULL; //puntero a la cola de eventos
    ALLEGRO_TIMER *timer = NULL; //puntero a timer
    
    /*Variables de estado*/
    bool parpadea = false;
    bool modoB=false;
    bool redraw = false;
    bool display_close = false;
    
    if (!al_init()) //inicializamos Allegro 
    {
        fprintf(stderr, "Error al inicializar alegro!\n");
        return -1;
    }

    if (!al_install_keyboard()) {
        fprintf(stderr, "failed to initialize the keyboard!\n");
        return -1;
    }


     timer = al_create_timer(1.0 / FPS); //crea el timer pero NO empieza a correr
     
    if (!timer) //verificamos que se haya creado el timer
    {
        fprintf(stderr, "Fallo al crear el timer!\n");
        return -1;
    }

    event_queue = al_create_event_queue(); //creamos la cola de eventos
    
    if (!event_queue) 
    {
        fprintf(stderr, "Falla al crear la cola de eventos!\n");
        al_destroy_timer(timer);
        return -1;
    }

    display = al_create_display(1000, 700 );//creamos un display de 1000 x 700
    
    if (!display)//si el display no se creo... 
    {
        fprintf(stderr, "Falla al crear el display!\n");
        al_destroy_event_queue(event_queue);
        al_destroy_timer(timer);
        return -1;
    }
    
    al_init_primitives_addon(); //inicializamos las primitivas (circulos, elipses, etc
    
    /*EVENTOS*/
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    
    /*INICIALIZACIÓN DEL DISPLAY*/
    al_clear_to_color(al_map_rgb(0, 0, 0));
    al_flip_display();

    al_start_timer(timer); //comienza el timer
        
    while (!display_close) //si no se presiona el boton de cerrar ventana...
    {
        ALLEGRO_EVENT ev; //definimos la variable para eventos
        if (al_get_next_event(event_queue, &ev)) //Toma un evento de la cola, VER RETURN EN DOCUMENT.
        {
            if (ev.type == ALLEGRO_EVENT_TIMER)
            {
                if(modoB){
                parpadea = ~parpadea;   //SI modo B esta encendido, la variable parpadea oscilara entre 1 y 0
                redraw = true;
                }
                else {                  //Si modo B esta apagado, la variable parpadea queda fija en 1
                    parpadea=true;
                    redraw=true;        //este redraw hace que se dibuje TODO cada el tiempo del timer
                }
                
            }
            else if()   //ACA PONER LAS CONDICIONES PARA CADA UNO DE LOS NUMEROS, CON SU BITSET 
            else if (ev.type == ALLEGRO_EVENT_KEY_T) //Cuando ingreso una T
            {
                maskToggle(A,0xFF); //Reemplaza TODOS los leds por el estado opuesto
                redraw==true;
            }
            else if (ev.type == ALLEGRO_EVENT_KEY_C)    //Cuando ingreso una C
            {
                maskOff(A, 0xFF);                       //Apaga TODOS los leds
                redraw==true;                       
            }
            else if (ev.type == ALLEGRO_EVENT_KEY_S)     //Cuando ingreso una s
            {
                maskOn(A, 0xFF);                    //Enciendo TODOS los leds
                redraw==true;            
            }
            else if (ev.type == ALLEGRO_EVENT_KEY_B)     //Cuando ingreso una b
            {
                modoB=~modoB;                   //Cada vez que ingrese una b, entra o sale del modo b (No me acuerdo si era ~para negar)
                redraw==true;            
            }
            else if (ev.type== ALLEGRO_EVENT_KEY_Q)
            {
                display_close=true;
            }

            
            else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
            {
                display_close = true;
            }
        }

        if (redraw)//si se redibuja los LEDs 
        {
            int i;
            for(i=0; i<=MAXBIT_AB; i++)
            {
                al_draw_filled_circle(COORD_Xi + i * COORD_X_STEP, COORD_Y, LED_RAD, (bitGet(A,MAXBIT_AB-i) && parpadea) ? al_color_name("red") : al_color_name("white")) ;
                /*Si parpadea esta en 1, solo dependera del bitget de cada bit, si parpadea esta en 0 se veran todos apagados sin importar su getbit,
                y a su vez, cuando sea necesario parpadea oscila entre 1 y 0 haciendo parpadear aquellos leds que esten encendidos*/
            }
            al_flip_display();
            redraw = false;
            
        }

    }
    
    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);

   
    
    return (EXIT_SUCCESS);
}

